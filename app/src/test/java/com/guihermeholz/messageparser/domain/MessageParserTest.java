package com.guihermeholz.messageparser.domain;

import com.guihermeholz.messageparser.model.MessageLink;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class MessageParserTest {

    @Mock
    OkHttpClient client;

    @Mock
    MessageLinkLoader loader;

    private MessageParser parser;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        parser = new MessageParser(loader);
    }

    @Test
    public void shouldParseMentionsCorrectly() {
        List<String> result = parser.parseMentions("@test, @another");
        assertEquals(result.size(), 2);
        assertEquals(result.get(0), "test");
        assertEquals(result.get(1), "another");
    }

    @Test
    public void shouldParseEmoticonsCorrectly() {
        List<String> result = parser.parseEmoticons("(test), (another)");
        assertEquals(result.size(), 2);
        assertEquals(result.get(0), "test");
        assertEquals(result.get(1), "another");
    }

    @Test
    public void shouldParseLinksCorrectly() throws IOException {
        when(loader.loadPageTitle(anyString())).thenReturn("Test title");
        List<MessageLink> result = parser.parseLinks("http://www.test.com, https://another.org");
        assertEquals(result.size(), 2);
        assertEquals(result.get(0).url, "http://www.test.com");
        assertEquals(result.get(1).url, "https://another.org");
    }
}
