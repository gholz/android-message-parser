package com.guihermeholz.messageparser.viewmodel;

import com.guihermeholz.messageparser.domain.MessageParser;
import com.guihermeholz.messageparser.utils.RxTestingUtils;
import com.guihermeholz.messageparser.view.MessageInputView;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Observable;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MessageInputViewModelTest {

    @Mock
    MessageInputView view;
    @Mock
    MessageParser parser;

    private MessageInputViewModel viewModel;

    @BeforeClass
    public static void init() {
        RxTestingUtils.setTestSchedulers();
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        viewModel = new MessageInputViewModel(view, parser);
    }

    @Test
    public void shouldDisplayErrorOnParsingError() {
        when(parser.parse(anyString()))
                .thenReturn(Observable.error(new RuntimeException("test")));
        viewModel.messageInput.set("");
        viewModel.parseMessage();
        verify(view).displayError(anyString());
    }

    @Test
    public void shouldHideKeyboard() {
        when(parser.parse(anyString()))
                .thenReturn(Observable.just(""));
        viewModel.messageInput.set("");
        viewModel.parseMessage();
        verify(view).hideKeyboard();
    }

    @Test
    public void shouldDisplayParsedContent() {
        String input = "@test, @another";
        String output = "{mentions:[test,another]}";

        when(parser.parse(input))
                .thenReturn(Observable.just(output));

        viewModel.messageInput.set(input);
        viewModel.parseMessage();

        assertEquals(viewModel.messageOutput.get(), input);
        assertEquals(viewModel.parsedOutput.get(), output);
    }
}
