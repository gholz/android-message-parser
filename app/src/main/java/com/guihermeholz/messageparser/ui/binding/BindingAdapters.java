package com.guihermeholz.messageparser.ui.binding;

import android.databinding.BindingConversion;
import android.view.View;

public class BindingAdapters {
    @BindingConversion
    public static int convertBooleanToVisibility(boolean visible) {
        return visible ? View.VISIBLE : View.GONE;
    }
}
