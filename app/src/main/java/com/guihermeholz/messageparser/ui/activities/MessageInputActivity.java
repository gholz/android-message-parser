package com.guihermeholz.messageparser.ui.activities;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.guihermeholz.messageparser.MessageParserApplication;
import com.guihermeholz.messageparser.R;
import com.guihermeholz.messageparser.databinding.MessageInputBinding;
import com.guihermeholz.messageparser.domain.MessageParser;
import com.guihermeholz.messageparser.view.MessageInputView;
import com.guihermeholz.messageparser.viewmodel.MessageInputViewModel;

import javax.inject.Inject;

public class MessageInputActivity extends AppCompatActivity implements MessageInputView {

    @Inject
    MessageParser parser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MessageParserApplication.getComponent().inject(this);
        MessageInputBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setViewModel(new MessageInputViewModel(this, parser));
    }

    @Override
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void displayError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
