package com.guihermeholz.messageparser.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

@JsonObject
public class MessageContents {
    @JsonField(name = "mentions")
    public List<String> mentions;
    @JsonField(name = "emoticons")
    public List<String> emoticons;
    @JsonField(name = "links")
    public List<MessageLink> links;
}
