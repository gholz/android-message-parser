package com.guihermeholz.messageparser.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class MessageLink {
    @JsonField(name = "url")
    public String url;
    @JsonField(name = "title")
    public String title;

    public MessageLink(String url, String title) {
        this.url = url;
        this.title = title;
    }

    public MessageLink() {
        //for serializing only
    }
}
