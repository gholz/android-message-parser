package com.guihermeholz.messageparser.view;

public interface MessageInputView {
    void hideKeyboard();

    void displayError(String message);
}
