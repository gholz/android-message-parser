package com.guihermeholz.messageparser.di;

import com.guihermeholz.messageparser.ui.activities.MessageInputActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(MessageInputActivity activity);
}
