package com.guihermeholz.messageparser.di;

import com.guihermeholz.messageparser.domain.MessageLinkLoader;
import com.guihermeholz.messageparser.domain.MessageParser;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

@Module
public class ApplicationModule {

    @Provides
    @Singleton
    OkHttpClient providesHttpClient() {
        return new OkHttpClient.Builder().build();
    }

    @Provides
    @Singleton
    MessageParser providesMessageParser(MessageLinkLoader loader) {
        return new MessageParser(loader);
    }

    @Provides
    @Singleton
    MessageLinkLoader provideMessageLinkLoader(OkHttpClient client) {
        return new MessageLinkLoader(client);
    }
}
