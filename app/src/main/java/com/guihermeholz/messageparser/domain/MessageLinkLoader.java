package com.guihermeholz.messageparser.domain;

import android.util.Log;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MessageLinkLoader {

    private static final Pattern PATTERN_TITLE = Pattern.compile("<title>.+</title>", Pattern.CASE_INSENSITIVE);
    private static final String LOG_TAG = MessageLinkLoader.class.getSimpleName();

    private OkHttpClient client;

    public MessageLinkLoader(OkHttpClient client) {
        this.client = client;
    }

    String loadPageTitle(String url) {
        Request request = new Request.Builder().url(url).build();
        try {
            Response response = client.newCall(request).execute();
            if (response.body() != null) {
                String body = response.body().string();
                return parsePageTitle(body);
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "error loading page", e);
            return "";
        }

        return "";
    }

    private String parsePageTitle(String body) {
        Matcher matcher = PATTERN_TITLE.matcher(body);
        if (matcher.find()) {
            return matcher.group().replaceAll("(?i)<title>|</title>", "");
        }

        return "";
    }
}
