package com.guihermeholz.messageparser.domain;

import android.support.annotation.VisibleForTesting;

import com.bluelinelabs.logansquare.LoganSquare;
import com.guihermeholz.messageparser.model.MessageContents;
import com.guihermeholz.messageparser.model.MessageLink;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Observable;

public class MessageParser {

    private static final Pattern PATTERN_MENTIONS = Pattern.compile("@\\w+(\\W|$)");
    private static final Pattern PATTERN_EMOTICONS = Pattern.compile("\\(\\w+\\)");
    private static final Pattern PATTERN_URLS = Pattern.compile("https?://(www\\.)?[-\\w@:%._+~#=]{2,256}\\.[a-z]{2,6}\\b([-\\w@:%_+.~#?&/=]*)");

    private MessageLinkLoader linkLoader;

    public MessageParser(MessageLinkLoader linkLoader) {
        this.linkLoader = linkLoader;
    }

    public Observable<String> parse(String message) {
        return Observable.just(message)
                .map(this::parseContents)
                .map(LoganSquare::serialize);
    }

    @VisibleForTesting
    MessageContents parseContents(String message) throws IOException {
        MessageContents contents = new MessageContents();
        contents.mentions = parseMentions(message);
        contents.emoticons = parseEmoticons(message);
        contents.links = parseLinks(message);

        return contents;
    }

    @VisibleForTesting
    List<String> parseMentions(String message) {
        List<String> mentions = new ArrayList<>();
        Matcher matcher = PATTERN_MENTIONS.matcher(message);
        while (matcher.find()) {
            mentions.add(matcher.group().replaceAll("(@|\\W)", ""));
        }

        return mentions.isEmpty() ? null : mentions;
    }

    @VisibleForTesting
    List<String> parseEmoticons(String message) {
        List<String> emoticons = new ArrayList<>();
        Matcher matcher = PATTERN_EMOTICONS.matcher(message);
        while (matcher.find()) {
            emoticons.add(matcher.group().replaceAll("(\\(|\\))", ""));
        }

        return emoticons.isEmpty() ? null : emoticons;
    }

    @VisibleForTesting
    List<MessageLink> parseLinks(String message) throws IOException {
        List<MessageLink> links = new ArrayList<>();
        Matcher matcher = PATTERN_URLS.matcher(message);
        while (matcher.find()) {
            String url = matcher.group();
            links.add(new MessageLink(url, linkLoader.loadPageTitle(url)));
        }

        return links.isEmpty() ? null : links;
    }


}
