package com.guihermeholz.messageparser;

import android.app.Application;

import com.guihermeholz.messageparser.di.ApplicationComponent;
import com.guihermeholz.messageparser.di.ApplicationModule;
import com.guihermeholz.messageparser.di.DaggerApplicationComponent;

public class MessageParserApplication extends Application {

    private static ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule())
                .build();
    }

    public static ApplicationComponent getComponent() {
        return component;
    }
}
