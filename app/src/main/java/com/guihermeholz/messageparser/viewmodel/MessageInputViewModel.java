package com.guihermeholz.messageparser.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.guihermeholz.messageparser.domain.MessageParser;
import com.guihermeholz.messageparser.view.MessageInputView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MessageInputViewModel extends BaseObservable {

    private static final String LOG_TAG = MessageInputViewModel.class.getSimpleName();

    public ObservableField<String> messageInput = new ObservableField<>();
    public ObservableField<String> parsedOutput = new ObservableField<>();
    public ObservableField<String> messageOutput = new ObservableField<>();
    public ObservableBoolean showProgress = new ObservableBoolean(false);

    private MessageInputView view;
    private MessageParser parser;

    public MessageInputViewModel(MessageInputView view, MessageParser parser) {
        this.view = view;
        this.parser = parser;
    }

    public void parseMessage() {

        final String message = messageInput.get();

        messageInput.set(null);
        parsedOutput.set(null);

        messageOutput.set(message);
        showProgress.set(true);
        view.hideKeyboard();

        parser.parse(message)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(parsed -> {
                    parsedOutput.set(parsed);
                    showProgress.set(false);
                }, error -> {
                    showProgress.set(false);
                    view.displayError("An error has occurred");
                });
    }
}
